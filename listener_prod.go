// +build prod
// +build !spacemonkey

package main

import (
	"github.com/shanemhansen/gossl"
	"net"
)

//Listening using CGO
func Listen(confs *HostConf) net.Listener {
	port := confs.ListenPort

	ctx := gossl.NewContext(gossl.TLSv1_2ServerMethod())
	ctx.SetOptions(gossl.OpNoCompression)

	err := ctx.UsePrivateKeyFile(confs.Certificate, gossl.FileTypePem)
	if err != nil {
		panic(err)
	}

	ctx.UseCertificateFile(confs.Certificate, gossl.FileTypePem)
	if err != nil {
		panic(err)
	}

	LOG.Infof("[dev]Starting TLS Listener on port: %s", port)
	ln, err := net.Listen("tcp", ":"+port)

	if err != nil {
		panic(err)
	}

	ln, err = gossl.NewListener(ln, ctx)
	if err != nil {
		panic(err)
	}

	return ln
}

prod:
	@BUILD_ENV=prod ./build.sh

spacemonkey:
	@BUILD_ENV=spacemonkey ./build.sh

darwin64:
	@./build.sh darwin amd64
all:
	@./build.sh


# vim:ft=make

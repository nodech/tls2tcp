#!/bin/bash

CMD="go"
PWD=`pwd`
BUILD_DIR="$PWD/bin"
LDFLAGS="-w -X main.minversion=`date -u +.%Y%m%d.%H%M%S`"
TAGS=""

PROJECT="gitlab.com/nodech/tls2tcp"
NAME="tls2tcp"

echo "Build Dir: ${BUILD_DIR}"
echo "For Project: ${PROJECT}"

OSES=(linux darwin)
ARCHS=(amd64)

build_specific() {
  BINNAME="${NAME}_${1}_$2_${BUILD_ENV}"

  if [ "$OS" == "windows" ]; then
    BINNAME="${BINNAME}.exe"
  else
    BINNAME="${BINNAME}.run"
  fi

  echo "Build for: $1/$2, $BUILD_DIR/$BINNAME"
  GOOS=$1 GOARCH=$2 go build -ldflags "$LDFLAGS" -tags "$TAGS" -o "bin/$BINNAME"
  echo $cmd
}

build_all() {
  echo "Build all"
  for os in ${OSES[@]};do
    for arch in ${ARCHS[@]};do
      build_specific $os $arch
    done
  done
}

OS=$1
ARCH=$2

if [ "${BUILD_ENV}" != "" ]; then
  TAGS="${BUILD_ENV}"

  if [ "${BUILD_ENV}" == "spacemonkey" ];then
    TAGS="prod $TAGS"
  fi
fi

if [ "${BUILD_ENV}" == "prod" ] || [ "${BUILD_ENV}" == "spacemonkey" ]; then
  OS=$(uname -s | tr '[:upper:]' '[:lower:]')
  ARCH="amd64"
fi


if [ "$OS" = "" ] || [ "$ARCH" = "" ]; then
  build_all
else
  build_specific $OS $ARCH
fi


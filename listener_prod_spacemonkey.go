// +build prod
// +build spacemonkey

package main

import (
	"github.com/spacemonkeygo/openssl"
	"net"
)

//Listening using CGO
func Listen(confs *HostConf) net.Listener {
	port := confs.ListenPort

	ctx, err := openssl.NewCtxFromFiles(confs.Certificate, confs.PrivateKey)

	if err != nil {
		panic(err)
	}

	LOG.Infof("[dev]Starting TLS Listener on port: %s", port)
	ln, err := openssl.Listen("tcp", ":"+port, ctx)

	if err != nil {
		panic(err)
	}

	return ln
}

package main

import (
	"io"
	"os"

	"github.com/op/go-logging"
)

var logLevels = map[string]logging.Level{
	"DEBUG":    logging.DEBUG,
	"INFO":     logging.INFO,
	"NOTICE":   logging.NOTICE,
	"WARNING":  logging.WARNING,
	"ERROR":    logging.ERROR,
	"CRITICAL": logging.CRITICAL,
}

//LOG is application level logger
var LOG *logging.Logger

func initLogs(config *HostConf) {
	LOG = logging.MustGetLogger("tls2tcp")

	level, ok := logLevels[stdoutLogLevel]

	if !ok {
		level = logging.INFO
	}

	stdoutFormat := logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)

	stdout := logging.NewLogBackend(os.Stdout, "", 0)
	stdoutFormatted := logging.NewBackendFormatter(stdout, stdoutFormat)
	stdoutLeveled := logging.AddModuleLevel(stdoutFormatted)
	stdoutLeveled.SetLevel(level, "")

	logging.SetBackend(stdoutLeveled)

	if config.FileLog == "" {
		return
	}

	fileFormat := logging.MustStringFormatter(
		`%{time} %{level:.4s} %{message}`,
	)

	level, ok = logLevels[config.FileLogLevel]
	if !ok {
		panic("Could not find level: " + config.FileLogLevel)
	}

	file, err := openFile(config.FileLog)

	if err != nil {
		panic("Could not open file: " + config.FileLog)
	}

	fileout := logging.NewLogBackend(file, "", 0)
	fileoutFormatted := logging.NewBackendFormatter(fileout, fileFormat)
	fileoutLeveled := logging.AddModuleLevel(fileoutFormatted)

	fileoutLeveled.SetLevel(level, "")
	logging.SetBackend(stdoutLeveled, fileoutLeveled)
}

func openFile(filename string) (io.Writer, error) {
	var file *os.File

	finfo, err := os.Stat(filename)

	if finfo == nil || err != nil {
		//attempt to create file
		file, err = os.Create(filename)
	} else {
		file, err = os.OpenFile(filename, os.O_WRONLY, 0666)

		if err == nil {
			_, err = file.Seek(0, os.SEEK_END)
		}
	}

	return file, err
}

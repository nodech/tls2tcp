// +build !prod

package main

import (
	"crypto/tls"
	"net"
)

//Listen listens with crypto/tls
func Listen(confs *HostConf) net.Listener {
	port := confs.ListenPort

	cer, err := tls.LoadX509KeyPair(confs.Certificate, confs.PrivateKey)

	if err != nil {
		panic(err)
	}

	config := &tls.Config{Certificates: []tls.Certificate{cer}}

	LOG.Infof("[dev]Starting TLS Listener on port: %s", port)
	ln, err := tls.Listen("tcp", ":"+port, config)

	if err != nil {
		panic(err)
	}

	return ln
}

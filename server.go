package main

import (
	"io"
	"net"
)

//Server is the struct for actual server
type Server struct {
	Configs *HostConf
	Count   uint64
}

//Listen will listen with HostConf and accept connections
func (s *Server) Listen() {
	ln := Listen(s.Configs)

	LOG.Debugf("Accepting conns")
	s.Accept(ln)
}

//Accept will accept connection and create goroutine with Accepted fn
func (s *Server) Accept(ln net.Listener) {
	for {
		conn, err := ln.Accept()

		if err != nil {
			LOG.Error(err.Error())
			conn.Close()
			continue
		}

		s.Count++
		LOG.Infof("Accepted connection %d", s.Count)

		go s.Accepted(conn)
	}
}

//Accepted starts actual operations on open connection
func (s *Server) Accepted(conn net.Conn) {
	LOG.Debugf("Connecting: %s", s.Configs.RedirectHost+":"+s.Configs.RedirectPort)
	recon, err := net.Dial("tcp", s.Configs.RedirectHost+":"+s.Configs.RedirectPort)

	if err != nil {
		LOG.Error(err.Error())
		conn.Close()
		return
	}

	LOG.Debugf("Connected from: %s", recon.LocalAddr().String())

	go copyConn(conn, recon)
	go copyConn(recon, conn)
}

//NewServer creates new Server
func NewServer(conf *HostConf) *Server {
	serv := new(Server)
	serv.Configs = conf

	return serv
}

func copyConn(src, dst net.Conn) {
	buffer := make([]byte, 4096)
	srcAddr := src.LocalAddr().String()
	dstAddr := dst.LocalAddr().String()
	LOG.Debugf("Starting copy from: %s to %s", srcAddr, dstAddr)

	for {
		n, err := src.Read(buffer)

		if err == io.EOF {
			LOG.Debugf("Connection closed %s. Closing another: %s", srcAddr, dstAddr)
			dst.Close()
			break
		}

		if err != nil {
			opError, ok := err.(*net.OpError)

			if !ok || opError.Err.Error() != "use of closed network connection" {
				LOG.Warningf("Connection closed with err: %d", err.Error())
			}

			dst.Close()
			break
		}

		n, err = dst.Write(buffer[:n])

		if n > 0 {
			LOG.Debugf("Written %d bytes from %s to %s\n", n, srcAddr, dstAddr)
			LOG.Debugf("Data: %s", buffer[:n])
		}
	}
}

package main

import (
	"io/ioutil"

	"github.com/ghodss/yaml"
)

//HostConf is yaml configuration structure
type HostConf struct {
	Name         string
	ListenPort   string
	RedirectHost string
	RedirectPort string
	Certificate  string
	PrivateKey   string
	FileLog      string
	FileLogLevel string
}

func parseYaml(file string) (*HostConf, error) {
	configs := &HostConf{}

	data, err := ioutil.ReadFile(file)

	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(data, configs)

	return configs, err
}

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"time"
)

var configFile string
var stdoutLogLevel string
var cpuprofile string
var minversion string
var showVersion bool
var version string

func init() {
	flag.StringVar(&configFile, "conf", "./conf/default.yaml", "Config file to use")
	flag.StringVar(&stdoutLogLevel, "stdout-log", "INFO", "Stdout Logging level")
	flag.StringVar(&cpuprofile, "cpuprofile", "", "write cpu profile to file")
	flag.BoolVar(&showVersion, "version", false, "show version of build")
}

func main() {
	flag.Parse()

	version = buildEnv + baseVersion + minversion

	if showVersion {
		fmt.Println("build: " + version)
		return
	}

	if cpuprofile != "" {
		f, err := os.Create(cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)

		go func() {
			<-time.NewTimer(time.Second * 40).C
			pprof.StopCPUProfile()
		}()
	}

	configs, err := parseYaml(configFile)

	if err != nil {
		panic(err)
	}

	initLogs(configs)

	LOG.Infof("Starting server %s", configs.Name)

	serv := NewServer(configs)
	serv.Listen()
}
